import { useMutation } from "@tanstack/react-query";
import * as API from "@/utils/api";
import { useNavigate } from "react-router";
import { ModalInfomation } from "@/components/modal/informationModal";
import { useState } from "react";
import { AxiosError } from "axios";

export default function Login() {
  const { mutate: LOGIN, isLoading } = useMutation(API.Login);

  const [showModal, setShowModal] = useState(false);

  const [errorMessage, setErrorMessage] = useState("");

  const navigate = useNavigate();

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();

    const currentValue = e.target as HTMLInputElement & { value: string }[];

    const username = currentValue[0].value;
    const password = currentValue[1].value;

    LOGIN(
      {
        username,
        password,
      },
      {
        onSuccess: (data) => {
          navigate("/");
        },
        onError: (data) => {
          const error = data as AxiosError;

          setErrorMessage(error.response?.data as string);

          setShowModal(true);

          setTimeout(() => {
            setShowModal(false);
          }, 3000);
        },
      }
    );
  };

  return (
    <>
      {showModal ? <ModalInfomation message={errorMessage} /> : null}

      <div className="h-[100vh] w-full border-2 flex justify-center items-center">
        <div className="p-4 w-11/12 md:w-4/12 border rounded-md shadow-sm">
          <h1 className="text-lg font-bold mb-4">Welcome buddy :)</h1>
          <form onSubmit={handleSubmit} className="flex flex-col gap-2">
            <div className="flex flex-col gap-1" aria-label="username-field">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="username"
              >
                username
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="text"
                id="username"
                required
              />
            </div>
            <div className="flex flex-col gap-1" aria-label="password-field">
              <label
                className="text-sm font-light tracking-wide text-gray-700"
                htmlFor="password"
              >
                password
              </label>
              <input
                className="p-2 text-sm  rounded-md bg-gray-100 focus:border-blue-100"
                type="password"
                id="password"
                required
              />
            </div>
            <button
              disabled={isLoading}
              className="bg-gray-200 w-max px-4 py-2 rounded-md text-sm mt-2 active:bg-gray-300"
              type="submit"
            >
              {isLoading ? "loading.." : "Login"}
            </button>
          </form>
        </div>
      </div>
    </>
  );
}
