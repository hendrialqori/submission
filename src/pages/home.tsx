import { ModalInfomation } from "@/components/modal/informationModal";
import { TUser } from "@/types";
import * as API from "@/utils/api";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useState } from "react";
import { Link } from "react-router-dom";

export default function Home() {
  const [deleteInfo, setDeleteInfo] = useState({
    isModal: false,
    message: "",
  });

  const [idDelete, setIdDelete] = useState(0);

  const { data, isLoading } = useQuery<TUser[] | undefined>({
    queryKey: ["user"],
    queryFn: async () => await API.getAllUser(),
    keepPreviousData: true,
    cacheTime: 120000,
  });

  const { mutate: deleteUser, isLoading: laodingDeleteUser } = useMutation(
    API.deleteUser
  );

  const handleDelete = (id: number) => {
    setIdDelete(id);
    deleteUser(id, {
      onSuccess: (data) => {
        setDeleteInfo({
          isModal: true,
          message: `Success delete user with id ${id}`,
        });

        setTimeout(() => {
          setDeleteInfo((prev) => ({
            ...prev,
            isModal: false,
          }));
        }, 1500);
      },
      onError: (data) => {
        console.log(data);
      },
    });
  };

  return (
    <>
      {deleteInfo.isModal ? (
        <ModalInfomation message={deleteInfo.message} />
      ) : null}
      <div className="w-full flex flex-col justify-center items-center my-3">
        <div className="flex justify-between items-center w-11/12 md:w-4/12">
          <h1 className="text-lg font-bold my-4">List user</h1>
          <Link
            to={"/formulir"}
            className="bg-gray-200 w-max px-4 py-2 rounded-md text-sm active:bg-gray-300"
          >
            create user
          </Link>
        </div>
        <div className="p-4 w-11/12 md:w-4/12 flex flex-col gap-3">
          {isLoading ? (
            <p>get user ...</p>
          ) : (
            data &&
            data?.map((user) => (
              <div className="flex items-start justify-between" key={user.id}>
                <div>
                  <h1 className="font-semibold text-lg">
                    {user.name.firstname + " " + user.name.lastname}{" "}
                  </h1>
                  <p className="text-sm text-gray-600 font-light">
                    {user.username}
                  </p>
                </div>
                <div>
                  <Link
                    to={`/formulir?idUpdate=${user.id}`}
                    className="bg-gray-200 w-max px-4 py-2 rounded-md text-sm mt-2 active:bg-gray-300 mr-3"
                  >
                    Update
                  </Link>
                  <button
                    disabled={laodingDeleteUser}
                    onClick={() => handleDelete(user.id)}
                    className="bg-gray-200 w-max px-4 py-[.40rem] rounded-md text-sm mt-2 active:bg-gray-300"
                  >
                    {laodingDeleteUser && user.id === idDelete
                      ? "loading..."
                      : "Delete"}
                  </button>
                </div>
              </div>
            ))
          )}
        </div>
      </div>
    </>
  );
}
