import { useForm } from "react-hook-form";
import type { TForm, TUser } from "@/types";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import * as API from "@/utils/api";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { ModalInfomation } from "@/components/modal/informationModal";

export default function Formulir() {
  const [query] = useSearchParams();

  const idUpdate = query.get("idUpdate");

  const navigate = useNavigate();

  const [createInfo, setCreatInfo] = useState({
    isModal: false,
    message: "",
  });

  const [updateinfo, setUpdateInfo] = useState({
    isModal: false,
    message: "",
  });

  const { register, setValue, handleSubmit } = useForm<TForm>({
    defaultValues: {
      zipcode: !idUpdate ? "12926-3874" : "",
      lat: "-37.3159",
      long: "81.1496",
      phone: "1-570-236-7033",
    },
  });

  const { data: singleUser, isLoading: loadingGetSingleUser } = useQuery<
    TUser | undefined
  >({
    queryKey: ["user"],
    queryFn: async () => await API.getSingleUser(Number(idUpdate)),
    enabled: !!idUpdate!,
  });

  useEffect(() => {
    if (singleUser) {
      setValue("email", singleUser?.email);
      setValue("username", singleUser?.username);
      setValue("password", singleUser?.password);
      setValue("firstname", singleUser?.name?.firstname);
      setValue("lastname", singleUser?.name?.lastname);
      setValue("city", singleUser?.address?.city);
      setValue("street", singleUser?.address?.street);
      setValue("number", singleUser?.address?.number);
      setValue("zipcode", singleUser?.address?.zipcode);
      setValue("lat", singleUser?.address?.geolocation?.lat);
      setValue("long", singleUser?.address?.geolocation?.long);
      setValue("phone", singleUser?.phone);
    }
  }, [singleUser]);

  const { mutate: createUser, isLoading: loadingCreateUser } = useMutation(
    API.CreateUser
  );

  const { mutate: updateUser, isLoading: loadingUpdateUser } = useMutation(
    API.UpdateUser
  );

  const submit = handleSubmit((data) => {
    if (!!idUpdate) {
      updateUser(
        {
          id: singleUser?.id! || Number(idUpdate),
          data: data,
        },
        {
          onSuccess: (data) => {
            setUpdateInfo({
              isModal: true,
              message: `Success update user with id ${idUpdate}`,
            });

            setTimeout(() => {
              setUpdateInfo((prev) => ({
                ...prev,
                isModal: false,
              }));
              navigate("/");
            }, 1500);
          },
          onError: (data) => {
            console.log(data);
          },
        }
      );
    } else {
      createUser(data, {
        onSuccess: (data) => {
          setCreatInfo({
            isModal: true,
            message: `Success create user`,
          });

          setTimeout(() => {
            setCreatInfo((prev) => ({
              ...prev,
              isModal: false,
            }));
            navigate("/");
          }, 1500);
        },
        onError: (data) => {
          console.log(data);
        },
      });
    }
  });

  return (
    <>
      {createInfo.isModal ? (
        <ModalInfomation message={createInfo.message} />
      ) : null}

      {updateinfo.isModal ? (
        <ModalInfomation message={updateinfo.message} />
      ) : null}
      <div className="w-full flex justify-center items-center my-5">
        <div className="p-4 w-11/12 md:w-4/12">
          <button
            onClick={() => {
              navigate("/");
              window.location.reload();
            }}
            className="bg-gray-200 w-max px-4 py-2 rounded-md text-sm mt-2 active:bg-gray-300"
          >
            Back to home
          </button>
          <h1 className="text-lg font-bold my-4">
            {idUpdate ? "Update user" : "Create new user"}{" "}
          </h1>
          <form onSubmit={submit} className="flex flex-col gap-2">
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="email"
              >
                email
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="email"
                id="email"
                {...register("email", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="username"
              >
                username
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="text"
                id="username"
                {...register("username", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide text-gray-700"
                htmlFor="password"
              >
                password
              </label>
              <input
                className="p-2 text-sm  rounded-md bg-gray-100 focus:border-blue-100"
                type="password"
                id="password"
                {...register("password", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="firstname"
              >
                firstname
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="text"
                id="firstname"
                {...register("firstname", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="lastname"
              >
                lastname
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="text"
                id="lastname"
                {...register("lastname", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="city"
              >
                city
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="text"
                id="city"
                {...register("city", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="street"
              >
                street
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="text"
                id="street"
                {...register("street", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="number"
              >
                address number
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="number"
                id="number"
                {...register("number", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="zipcode"
              >
                zipcode
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="string"
                id="zipcode"
                {...register("zipcode", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label className="text-sm font-light tracking-wide" htmlFor="lat">
                lat
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="string"
                id="lat"
                {...register("lat", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="long"
              >
                long
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="string"
                id="long"
                {...register("long", {
                  required: true,
                })}
              />
            </div>
            <div className="flex flex-col gap-1">
              <label
                className="text-sm font-light tracking-wide"
                htmlFor="phone"
              >
                phone
              </label>
              <input
                className="p-2 text-sm rounded-md bg-gray-100 focus:border-blue-100"
                type="string"
                id="phone"
                {...register("phone", {
                  required: true,
                })}
              />
            </div>
            {idUpdate ? (
              <button
                disabled={loadingUpdateUser}
                className="bg-gray-200 w-max px-4 py-2 rounded-md text-sm mt-2 active:bg-gray-300"
                type="submit"
              >
                {loadingUpdateUser ? "loading.." : "update"}
              </button>
            ) : (
              <button
                disabled={loadingCreateUser}
                className="bg-gray-200 w-max px-4 py-2 rounded-md text-sm mt-2 active:bg-gray-300"
                type="submit"
              >
                {loadingCreateUser ? "loading.." : "create"}
              </button>
            )}
          </form>
        </div>
      </div>
    </>
  );
}
