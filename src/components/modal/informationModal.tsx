import { ModalWrapper } from "./modalWrapper";

type Props = {
  message: string;
};

export const ModalInfomation: React.FC<Props> = ({ message }) => {
  return (
    <ModalWrapper>
      <div
        className="w-4/12 rounded-md bg-white p-7 shadow-md"
        aria-label="modal"
      >
        <p>{message}</p>
      </div>
    </ModalWrapper>
  );
};
