import { lazy, Suspense } from "react";
import { Routes, Route, Outlet } from "react-router-dom";

const Login = lazy(async () => await import("@/pages/login"));
const Home = lazy(async () => await import("@/pages/home"));
const Formulir = lazy(async () => await import("@/pages/formulir"));

const LazyWrapper = () => {
  return (
    <Suspense fallback={<p>Loading ...</p>}>
      <Outlet />
    </Suspense>
  );
};

function App() {
  return (
    <Routes>
      <Route path="/" element={<LazyWrapper />}>
        <Route index element={<Home />} />
        <Route path="login" element={<Login />} />
        <Route path="formulir" element={<Formulir />} />
      </Route>
    </Routes>
  );
}

export default App;
