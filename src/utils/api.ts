import axios from "axios";
import type { TForm } from "@/types";

const ENDPOINT = "https://fakestoreapi.com";

export const Login = async (data: { username: string; password: string }) => {
  const req = await axios.post(`${ENDPOINT}/auth/login`, {
    username: data.username,
    password: data.password,
  });

  return await req.data;
};

export const getAllUser = async () => {
  const req = await axios.get(`${ENDPOINT}/users`);

  return await req.data;
};

export const getSingleUser = async (id: number) => {
  const req = await axios.get(`${ENDPOINT}/users/${id}`);

  return await req.data;
};

export const CreateUser = async (data: TForm) => {
  const req = await axios.post(`${ENDPOINT}/users`, {
    email: data.email,
    username: data.username,
    password: data.password,
    name: {
      firstname: data.firstname,
      lastname: data.lastname,
    },
    address: {
      city: data.city,
      street: data.street,
      number: data.number,
      zipcode: data.zipcode,
      geolocation: {
        lat: data.lat,
        long: data.long,
      },
    },
    phone: data.phone,
  });

  return await req.data;
};

export const UpdateUser = async ({ id, data }: { id: number; data: TForm }) => {
  const req = await axios.put(`${ENDPOINT}/users/${id}`, {
    email: data.email,
    username: data.username,
    password: data.password,
    name: {
      firstname: data.firstname,
      lastname: data.lastname,
    },
    address: {
      city: data.city,
      street: data.street,
      number: data.number,
      zipcode: data.zipcode,
      geolocation: {
        lat: data.lat,
        long: data.long,
      },
    },
    phone: data.phone,
  });

  return await req.data;
};

export const deleteUser = async (id: number) => {
  const req = await axios(`${ENDPOINT}/users/${id}`);

  return await req.data;
};
